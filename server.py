import pymorphy2
from aiohttp import web
from processor import process_articles, get_charged_words


async def handle(request):
    urls_str = request.query.get("urls")
    articles_urls = urls_str.split(",")

    if len(articles_urls) > 10:
        return web.json_response({"error": "too many urls in request, should be 10 or less"}, status=400)

    results = await process_articles(articles_urls, _morph=morph, _charged_words=charged_words)
    return web.json_response(results)


if __name__ == '__main__':
    morph = pymorphy2.MorphAnalyzer()
    charged_words = get_charged_words()

    app = web.Application()

    app.add_routes([
        web.get('/', handle),
    ])

    web.run_app(app)
