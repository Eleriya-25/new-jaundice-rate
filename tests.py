from unittest.mock import patch

import pytest

from main import process_articles, ProcessingStatus


class TestProcessArticles:
    @staticmethod
    def check_result(results, status):
        assert results
        assert len(results) == 1

        result = results[0]
        assert result
        assert result["status"] == status.value, f'text_process_time: {result["text_process_time"]}'

    @pytest.mark.asyncio
    async def test_bad_url(self):
        results = await process_articles(["some-bad-url"])
        self.check_result(results, ProcessingStatus.FETCH_ERROR)

    @pytest.mark.asyncio
    async def test_bad_article(self):
        results = await process_articles(["https://lenta.ru/brief/2021/08/26/afg_terror/"])
        self.check_result(results, ProcessingStatus.PARSING_ERROR)

    @patch('main.FETCH_TIMEOUT', 1)
    @pytest.mark.asyncio
    async def test_fetch_timeout(self):
        results = await process_articles(["https://inosmi.ru/20230206/sholts-260387982.html"])
        self.check_result(results, ProcessingStatus.TIMEOUT)

    @patch('main.TEXT_PROCESS_TIMEOUT', 1)
    @pytest.mark.asyncio
    async def test_text_process_timeout(self):
        results = await process_articles(["https://inosmi.ru/20230206/sholts-260387982.html"])
        self.check_result(results, ProcessingStatus.TIMEOUT)
