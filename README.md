# Фильтр желтушных новостей

[TODO. Опишите проект, схему работы]

Пока поддерживается только один новостной сайт - [ИНОСМИ.РУ](https://inosmi.ru/). Для него разработан специальный адаптер, умеющий выделять текст статьи на фоне остальной HTML разметки. Для других новостных сайтов потребуются новые адаптеры, все они будут находиться в каталоге `adapters`. Туда же помещен код для сайта ИНОСМИ.РУ: `adapters/inosmi_ru.py`.

В перспективе можно создать универсальный адаптер, подходящий для всех сайтов, но его разработка будет сложной и потребует дополнительных времени и сил.

## Перед запуском

Установка poetry - `pip3 install poetry`. Poetry - менеджер зависимостей.
Все зависимости перечислены в файле pyproject.toml.

# Как запустить

```python3
poetry run processor.py
```

```python3
poetry run server.py
```

# Как запустить тесты

Для тестирования используется [pytest](https://docs.pytest.org/en/latest/), тестами покрыты фрагменты кода сложные в отладке: text_tools.py и адаптеры. Команды для запуска тестов:

```
poetry run pytest adapters/inosmi_ru.py
```

```
poetry run pytest text_tools.py
```

```
poetry run pytest tests.py
```
