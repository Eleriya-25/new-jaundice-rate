import logging
import os
import time
from enum import Enum

import aiohttp
import asyncio

from aiohttp import ClientError
from anyio import create_task_group

import pymorphy2
from async_timeout import timeout

from adapters import ArticleNotFound
from adapters.inosmi_ru import sanitize
from text_tools import calculate_jaundice_rate, split_by_words_async


class ProcessingStatus(Enum):
    OK = 'OK'
    FETCH_ERROR = 'FETCH_ERROR'
    TIMEOUT = 'TIMEOUT'
    PARSING_ERROR = 'PARSING_ERROR'


TEST_ARTICLES = [
    "https://inosmi.ru/20230206/sholts-260387982.html",
    "https://inosmi.ru/20230206/evrokomissiya-260381212.html",
    "https://lenta.ru/brief/2021/08/26/afg_terror/",
    "https://inosmi.ru/20230206/ssha-260376601.html",
    "https://inosmi.ru/20230206/bennet-260382733.html",
    "https://inosmi.ru/20230206/basketbolist-260378360.html",
    "https://inosmi.ru/20230206/mchs-260390232.html",
]


FETCH_TIMEOUT = 100
TEXT_PROCESS_TIMEOUT = 3


async def fetch(session, url):
    async with timeout(FETCH_TIMEOUT):
        async with session.get(url) as response:
            response.raise_for_status()
            return await response.text()


def get_charged_words():
    charged_words = []

    for file_name in os.listdir("./charged_dict"):
        with open(f"./charged_dict/{file_name}") as file_obj:
            file_text = file_obj.read()
            words = [x.strip() for x in file_text.split("\n") if x]
            charged_words.extend(words)

    return charged_words


async def process_article_text(text, morph, charged_words):
    success, words, score = None, None, None

    text_process_start = time.monotonic()

    try:
        async with timeout(TEXT_PROCESS_TIMEOUT):
            words = await split_by_words_async(morph, text)
            score = calculate_jaundice_rate(words, charged_words)
            success = True
    except asyncio.TimeoutError:
        success = False

    text_process_end = time.monotonic()

    return success, words, score, text_process_end-text_process_start


async def process_article(article_url, session, morph, charged_words, results):
    def save_result(status, score=None, words=None, text_process_time=None):
        results.append({
            "article_url": article_url,
            "status": status.value,
            "score": score,
            "len_words": len(words) if words is not None else None,
            "text_process_time": text_process_time,
        })

    try:
        html = await fetch(session, article_url)
        clean_text = sanitize(html, plaintext=True)
        success, words, score, text_process_time = await process_article_text(clean_text, morph, charged_words)

        save_result(
            status=ProcessingStatus.OK if success else ProcessingStatus.TIMEOUT,
            score=score,
            words=words,
            text_process_time=text_process_time,
        )

    except ClientError:
        save_result(ProcessingStatus.FETCH_ERROR)

    except asyncio.TimeoutError:
        save_result(ProcessingStatus.TIMEOUT)

    except ArticleNotFound:
        save_result(ProcessingStatus.PARSING_ERROR)


async def process_articles(articles_urls, _morph, _charged_words):
    results = []

    async with aiohttp.ClientSession() as session:
        async with create_task_group() as tg:
            for article_url in articles_urls:
                tg.start_soon(process_article, article_url, session, _morph, _charged_words, results)

    return results


if __name__ == '__main__':
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    morph = pymorphy2.MorphAnalyzer()
    charged_words = get_charged_words()
    results = asyncio.run(process_articles(articles_urls=TEST_ARTICLES, _morph=morph, _charged_words=charged_words))

    for result in results:
        print(f"URL: {result['article_url']}")
        print(f"Статус: {result['status']}")
        print(f"Рейтинг: {result['score']}")
        print(f"Слов в статье: {result['len_words']}")

        if result["text_process_time"]:
            logging.info(f"Анализ закончен за {round(result['text_process_time'], 2)} сек")

        print()
